﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outletshop.Models.Repository;
using Outletshop.Models.Interface;
using Outletshop.Models.Database;

namespace Outletshop.Tests.Model
{
    class InMemoryProductsRepository : IProductsRepository
    {
        private List<Product> db = new List<Product>();
        private List<Consignment> db2 = new List<Consignment>();

        public Exception ExceptionToThrow { get; set; }

        public Consignment GetConsignmentById(int? id)
        {
            return db2.SingleOrDefault(p => p.consignmentId == id);
        }

        public Product GetProductById(int? id)
        {
            return db.FirstOrDefault(p => p.productId == id);
        }

        public void Add(Product product)
        {
            if (ExceptionToThrow != null)
                throw ExceptionToThrow;

            db.Add(product);
        }



        public void DeleteConsignment(int? id)
        {
            db.Remove(GetProductById(id));
        }

        public IEnumerable<Product> GetAllProducts()
        {
           return db.ToList();
        }

    

        public int SaveChanges()
        {
            return 1;
        }

        public void CreateConsignment(ConsignmentProduct viewModel, string filename)
        {
            throw new NotImplementedException();
        }
    }
}
