﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outletshop.Models.Interface;
using Outletshop.Controllers;
using Outletshop.Models.Database;

using System.Web.Routing;
using System.Web.Mvc;
using System.Web;
using Outletshop.Tests.Model;
using System.Collections.Generic;
using System.Linq;

namespace Outletshop.Tests.Controllers
{
    [TestClass]
    public class ProductsControllerTest
    {
        private static ProductsController GetProductsController(IProductsRepository proRepository)
        {
            ProductsController proController = new ProductsController(proRepository);
            proController.ControllerContext = new ControllerContext()
            {
                Controller = proController,
                RequestContext = new RequestContext(new MockHttpContext(), new RouteData())

            };

            return proController;
        }

        Product GetProductName(int pId, string pName, string pDescription)
        {
            return new Product
            {
                productId = pId,
                productName = pName,
                productDescription = pDescription
            };
        }


        // Gets product from repository
        [TestMethod]
        public void GetAllProductsFromRepository()
        {
            Product products1 = GetProductName(1, "Test produkt", "Beskrivelse");

            InMemoryProductsRepository proRepository = new InMemoryProductsRepository();

            proRepository.Add(products1);

            var controller = GetProductsController(proRepository);

            var result = controller.Index();

            var datamodel = ((ViewResult)result).ViewData.Model as IList<Product>;


            CollectionAssert.Contains(datamodel.ToList(), products1);

        }

        // Check if queal to.
        [TestMethod]
        public void View()
        {
            Product products1 = GetProductName(1, "Test produkt", "Beskrivelse");
            Assert.AreEqual("Test produkt" + "Beskrivelse", products1.productName + products1.productDescription);
        }






    }




    internal class MockHttpContext : HttpContextBase
    {
    }
}
