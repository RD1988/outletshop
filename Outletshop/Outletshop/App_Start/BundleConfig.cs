﻿using System.Web;
using System.Web.Optimization;

namespace Outletshop
{
    public class BundleConfig
    {
      
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/simple-sidebar.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
               "~/Scripts/dropzone/css/basic.css",
               "~/Scripts/dropzone/css/dropzone.css"));


            bundles.Add(new StyleBundle("~/Content/summernote").Include(
                "~/Content/summernote/summernote.css"
                ));

            bundles.Add(new ScriptBundle("~/bundle/summernote").Include(
           "~/Content/summernote/summernote.js"
           ));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                  "~/Scripts/dropzone/css/basic.css",
                  "~/Scripts/dropzone/css/dropzone.css"));

            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                     "~/Scripts/dropzone/dropzone.js"));



        }
    }
}
