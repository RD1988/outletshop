﻿using Outletshop.Controllers;
using Outletshop.Models.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using Outletshop.Models.Interface;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Repository
{
    public class OrderRepossitory : IOrderRepository
    {

        private Connection db = new Connection();


        public IEnumerable<Order> GetAllOrders()
        {

            return db.Orders.Include(x => x.orderItems).ToList();

        }

        public int SaveChanges()
        {
            return db.SaveChanges();
        }

    }
}