﻿using Outletshop.Models.Database;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Outletshop.Models.Interface;
using System;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Outletshop.Models.Repository
{
    public class ProductsRepository : IProductsRepository
    {
        private Connection db = new Connection();

        public void CreateConsignment(ConsignmentProduct viewModel, string filename)
        {
            var consignment = new Consignment()
            {
                Size = viewModel.Size,
                Color = viewModel.Color,
                stock = viewModel.stock,
                costPrice = viewModel.costPrice,
                salePrice = viewModel.salePrice,
                consignmentBrandId = viewModel.brandId
          
            };

            var product = new Product()
            {
                productName = viewModel.productName,
                productDescription = viewModel.productDescription,

            };
            db.Consignments.Add(consignment);
            db.Products.Add(product);
    
            db.SaveChanges();

        }

        public IEnumerable<Product> GetAllProducts()
        {
            return db.Products.Include(p => p.consignment).ToList();
        }
   
        // Get product by id
        public Product GetProductById(int? id)
        {
            return  db.Products.SingleOrDefault(p => p.productId == id);
        }

        // Get consignment by id
        public Consignment GetConsignmentById(int? id)
        {
            return db.Consignments.SingleOrDefault(p => p.consignmentId == id);
        }

        // delete the relation between consignment and the product
        public void DeleteConsignment(int? id)
        {
            var deleteProduct = GetConsignmentById(id);
            db.Consignments.Remove(deleteProduct);
            db.SaveChanges();
        }

        public int SaveChanges()
        {
            return db.SaveChanges();
        }

 


    }
}