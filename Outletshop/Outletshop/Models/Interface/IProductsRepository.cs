﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Outletshop.Models.Repository;
using Outletshop.Models.Database;
using Outletshop.Models.Interface;
using System.Threading.Tasks;

namespace Outletshop.Models.Interface
{
    public interface IProductsRepository
    {

        // get all products async
        IEnumerable<Product> GetAllProducts();


        void CreateConsignment(ConsignmentProduct viewModel, string filename);
        void DeleteConsignment(int? id);
        int SaveChanges();
        Product GetProductById(int? id);
        Consignment GetConsignmentById(int? id);
    }
}