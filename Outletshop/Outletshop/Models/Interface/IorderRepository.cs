﻿using Outletshop.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Interface
{
    public interface IOrderRepository
    {

         IEnumerable<Order> GetAllOrders();

        int SaveChanges();
    }
}