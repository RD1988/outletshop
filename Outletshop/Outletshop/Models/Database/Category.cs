﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public class Category
    {
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        private List<SubCategory> categories;

        public Category()
        {
            categories = new List<SubCategory>();
        }

        public virtual ICollection<SubCategory> subCategory { get; set; }
    }
}