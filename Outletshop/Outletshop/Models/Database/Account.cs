﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Outletshop.Models.Database
{
    public partial class Account
    {
        public int accountId { get; set; }

        public string accountMail { get; set; }

        public string accountName { get; set; }

        public string accountPassword { get; set; }

        public virtual Role role { get; set; }

      


    }
}