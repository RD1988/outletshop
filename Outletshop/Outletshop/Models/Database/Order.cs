﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public partial class Order
    {
        [Key, ForeignKey("orderItems")]
        public int orderId { get; set; }
        public DateTime? orderDate { get; set; }
   
        [DisplayName("Fornavn")]
        public string firstName { get; set; }
        [DisplayName("Efternavn")]
        public string lastName { get; set; }
        [DisplayName("E-mail adresse")]
        public string Email { get; set; }
        [DisplayName("Telefon nr")]
        public string phone { get; set; }
        [DisplayName("Postnr")]
        public string postalCode { get; set; }
        [DisplayName("By")]
        public string city { get; set; }


        public virtual ICollection<OrderItem> orderItems { get; set; }

        public virtual OrderItem orderItem { get; set; }

        public Order()
        {
            orderItems = new List<OrderItem>();

        }
        

    }
}