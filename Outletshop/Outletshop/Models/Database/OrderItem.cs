﻿using Outletshop.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public partial class OrderItem
    {
        public int orderItemId { get; set; }
        public string orderName { get; set; }
        public float salePrice { get; set; }
        public int quantity { get; set; }


    }
}