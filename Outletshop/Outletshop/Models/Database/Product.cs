﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Outletshop.Models.Database
{
    public partial class Product
    {
      
        public int productId { get; set; }
        [DisplayName("Produktnavn")]
        public string productName { get; set; }

        [DisplayName("Produktbeskrivelse")]
        public string productDescription { get; set; }

       


        // Et produkt kan associeres med  1 - 0..* billeder tilhørende det pågældende produkt

        //private List<Image> imgProducts;

        //public virtual ICollection<Image> images { get; set; }

        public virtual Consignment consignment { get; set; }

        public static implicit operator List<object>(Product v)
        {
            throw new NotImplementedException();
        }
    }
}