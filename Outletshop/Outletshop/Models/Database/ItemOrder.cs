﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public class ItemOrder
    {
        //############
        //Order
        //############

        public int orderId { get; set; }
        public DateTime? orderDate { get; set; }
        [DisplayName("Ordre detaljer")]
        public string orderText { get; set; }
        [DisplayName("Fornavn")]
        public string firstName { get; set; }
        [DisplayName("Efternavn")]
        public string lastName { get; set; }
        [DisplayName("E-mail adresse")]
        public string Email { get; set; }
        [DisplayName("Telefon nr")]
        public string phone { get; set; }
        [DisplayName("Postnr")]
        public string postalCode { get; set; }
        [DisplayName("By")]
        public string city { get; set; }

        //############
        //OrderItem
        //############

        public int orderItemId { get; set; }
        public string orderName { get; set; }
        public float salePrice { get; set; }
        public int quantity { get; set; }
    }
}