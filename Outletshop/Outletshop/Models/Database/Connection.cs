﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;



namespace Outletshop.Models.Database
{


    public partial class Connection : DbContext
    {
        public Connection() : base("Connection")
        {
        
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        //public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Consignment> Consignments { get; set; }

        public virtual DbSet<Brand> Brands { get; set; }
        //public virtual DbSet<Category> Categories { get; set; }
        //public virtual DbSet<SubCategory> SubCategories { get; set; }

        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }






        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Product>().HasKey(p => p.productId);

            modelBuilder.Entity<Consignment>().HasRequired(p => p.product).WithRequiredPrincipal(p => p.consignment).WillCascadeOnDelete(true);


        }
    }
}