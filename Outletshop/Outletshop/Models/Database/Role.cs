﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public  class Role
    {
      
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        private List<Account> roles;

        public Role()
        {
            roles = new List<Account>();
        }

        public virtual ICollection<Account> accounts { get; set; }

     
    }
}