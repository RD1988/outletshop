﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public class Brand
    {
        public int brandId { get; set; }
        public string brandName { get; set; }

    }
}