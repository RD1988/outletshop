﻿using System.ComponentModel;

namespace Outletshop.Models.Database
{

    public partial class Consignment
    {
        

        public int consignmentId { get; set; }

        [DisplayName("Lagerbeholdning")]
        public int stock { get; set; }

        [DisplayName("Købspris")]
        public float costPrice { get; set; }

        [DisplayName("Salgspris")]
        public float salePrice { get; set; }

        [DisplayName("Størrelse")]
        public string Size { get; set; }

        [DisplayName("Farve")]
        public string Color { get; set; }

        [DisplayName("Stylenr")]
        public int styleNumber { get; set; }

        [DisplayName("Produktkategori")]
        public int consignmentBrandId { get; set; }

   
        public virtual Product product { get; set; }



    }
}

