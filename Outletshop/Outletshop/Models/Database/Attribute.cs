﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public partial class Attribute
    {
        public int attributeId { get; set; }
        public int productId { get; set; }
        public string color { get; set; }
        public string width { get; set; }
        public string lenght { get; set; }
        public string stylenumber { get; set; }

    }
}