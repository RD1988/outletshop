﻿using System.Collections.Generic;
using System.ComponentModel;


namespace Outletshop.Models.Database
{
    public class ConsignmentProduct
    { 
        //############
        //Consignment
        //############

        public int consignmentId { get; set; }
        [DisplayName("Lagerbeholdning")]

        public int stock { get; set; }
        [DisplayName("Købspris")]

        public float costPrice { get; set; }
        [DisplayName("Salgspris")]

        public float salePrice { get; set; }
        [DisplayName("Størrelse")]

        public string Size { get; set; }
        [DisplayName("Farve")]
        public string Color { get; set; }

        [DisplayName("Stylenr")]
        public int styleNumber { get; set; }

        [DisplayName("Produktkategori")]
        public int consignmentBrandId { get; set; }
       
        //#######
        //Product
        //#######

        public int productId { get; set; }

        [DisplayName("Produkttitel")]
        public string productName { get; set; }

        [DisplayName("Produktbeskrivelse")]
        public string productDescription { get; set; }



        //#######
        //Images
        //#######

        public List<Consignment> consignment { get; set; }
        public List<Product> product { get; set; }

        //#######
        //Brands
        //#######


        public int brandId { get; set; }
        public string brandName { get; set; }


    }
}