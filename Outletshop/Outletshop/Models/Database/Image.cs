﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public partial class Image
    {
        public int imageId { get; set; }
        public string imageName { get; set; }
        public string imageAlt { get; set; }

        public virtual Product product { get; set; }


    }
}