﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Outletshop.Models.Database
{
    public class SubCategory
    {
        public int subCategoryId { get; set; }
        public string subCategoryName { get; set; }

        public virtual Category category { get; set; }
    }
}