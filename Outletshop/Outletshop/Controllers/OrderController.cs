﻿using Outletshop.Models.Database;
using Outletshop.Models.Interface;
using Outletshop.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Outletshop.Controllers
{
    public class OrderController : Controller
    {
        private Connection db = new Connection();

        IOrderRepository _repository;

        public OrderController() : this(new OrderRepossitory())
        {

        }

        public OrderController(IOrderRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            ViewData["ControllerName"] = this.ToString();
            return View("Index", _repository.GetAllOrders());
        }


    }
}