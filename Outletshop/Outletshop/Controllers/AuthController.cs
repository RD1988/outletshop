﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Outletshop.Models.Database;
using System.Web.Security;

namespace Outletshop.Controllers
{
    public class AuthController : Controller
    {
        private Connection db = new Connection();

        // GET: Auth login
        public ActionResult Logon()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Logon(Account model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                using (Connection db = new Connection())
                {
                    string accountMail = model.accountMail;
                    string accountPassword = model.accountPassword;

                    bool isUserValid = db.Account.Any(account => account.accountMail == accountMail && account.accountPassword == accountPassword);


                    // Checks value in the database is equal to accountName and accountPassword

                    if (isUserValid)
                    {
                        FormsAuthentication.SetAuthCookie(accountMail, false);
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                           && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Dashboard", "Account");
                        }

                    }

                    else
                    {
                        ModelState.AddModelError("", "Forkert e-mail eller adgangskode");
                    }




                }
            }

            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Logon", "Auth");
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }


        [HttpPost]
        public ActionResult ForgotPassword(Account model)
        {
            return View(model);
        }
    }
}