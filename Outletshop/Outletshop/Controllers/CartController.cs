﻿using Outletshop.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Outletshop.Controllers
{
    public class CartController : Controller
    {





        public Connection db = new Connection();


        // GET: Cart
        public ActionResult Order()
        {
            List<Item> cart = (List<Item>)Session["cart"];
            Session["cart"] = cart;

            return View();
        }
        public ActionResult Checkout(Order order)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            Session["cart"] = cart;

            return View();
        }

        public ActionResult Accept(Order order)
        {

            order.orderItems = new List<OrderItem>();
            var cart = (List<Item>)Session["cart"];


            foreach (var item in cart)
            {
                var ord = new Order()
                {

                    firstName = order.firstName,
                    lastName = order.lastName,
                    Email = order.Email,
                    phone = order.phone,
                    postalCode = order.postalCode,
                    city = order.city
                };


                var itemOrder = new OrderItem
                {

                    orderName = item.Product.productName,
                    salePrice = item.Product.consignment.salePrice,
                    quantity = item.Quantity,

                };


                ((List<OrderItem>)order.orderItems).Add(itemOrder);


            }

            db.Orders.Add(order);

            db.SaveChanges();

            return View();
        }



        private int isExisting(int? id)
        {
            List<Item> cart = (List<Item>)Session["cart"];

            if (cart.Count > 0)
            {
                for (int i = 0; i < cart.Count; i++)
                    if (cart[i].Product.productId == id)
                        return i;
            }

            if (cart.Count < 0)
            {
                for (int i = 0; i < cart.Count; i--)
                    if (cart[i].Product.productId == id)
                        return i;
            }


            return -1;
        }

        public ActionResult Delete(int? id)
        {
            int index = isExisting(id);
            List<Item> cart = (List<Item>)Session["cart"];
            Item item = cart.FirstOrDefault(l => l.Product.productId == id);


            if (item != null)
            {
                if (item.Quantity > 0)
                {
                    item.Quantity--;
                }

                if (item.Quantity == 0)
                {
                    cart.RemoveAt(index);
                }
            }

            Session["cart"] = cart;




            return RedirectToAction("Order");

        }





        public ActionResult OrderNow(int? id, string quantity)
        {

            if (id == null)
            {

                ViewBag.Message = "Kurvenen er tom!";

            }

            if (string.IsNullOrEmpty(Session["cart"] as string))
            {
                ViewBag.Message = "Kurvenen er tom!";
            }

            if (Session["cart"] == null)
            {

                List<Item> cart = new List<Item>();
                cart.Add(new Item(db.Products.Find(id), 1));

                Session["cart"] = cart;
            }

            else
            {
                List<Item> cart = (List<Item>)Session["cart"];
                int index = isExisting(id);

                if (index == -1)
                {
                    cart.Add(new Item(db.Products.Find(id), 1));

                }

                else
                {
                    cart[index].Quantity++;


                    Session["cart"] = cart;

                }




            }

            return RedirectToAction("Order");
        }





    }


}
