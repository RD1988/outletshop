﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Outletshop.Models.Database;
using System.Threading.Tasks;

namespace Outletshop.Controllers
{
    public class ConsignmentsController : Controller
    {
        private Connection db = new Connection();

        // GET: Consignments
        public async Task <ActionResult> Index()
        {
            var consignments = db.Consignments.Include(c => c.product);
          
            return View(await consignments.ToListAsync());
        }

        // GET: Consignments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = db.Consignments.Find(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            return View(consignment);
        }

        // GET: Consignments/Create
        public ActionResult Create()
        {
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName");
            return View();
        }

        // POST: Consignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "consignmentId,stock,costPrice,salePrice,consignmentBrandId")] Consignment consignment)
        {
            if (ModelState.IsValid)
            {
                db.Consignments.Add(consignment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // GET: Consignments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = db.Consignments.Find(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // POST: Consignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "consignmentId,stock,costPrice,salePrice,consignmentBrandId")] Consignment consignment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consignment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // GET: Consignments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = db.Consignments.Find(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            return View(consignment);
        }

        // POST: Consignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consignment consignment = db.Consignments.Find(id);
            db.Consignments.Remove(consignment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
