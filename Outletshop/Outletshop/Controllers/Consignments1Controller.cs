﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Outletshop.Models.Database;

namespace Outletshop.Controllers
{
    public class Consignments1Controller : Controller
    {
        private Connection db = new Connection();

        // GET: Consignments1
        public async Task<ActionResult> Index()
        {
            var consignments = db.Consignments.Include(c => c.product);
            return View(await consignments.ToListAsync());
        }

        // GET: Consignments1/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = await db.Consignments.FindAsync(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            return View(consignment);
        }

        // GET: Consignments1/Create
        public ActionResult Create()
        {
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName");
            return View();
        }

        // POST: Consignments1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "consignmentId,stock,costPrice,salePrice,Size,Color,styleNumber,consignmentBrandId")] Consignment consignment)
        {
            if (ModelState.IsValid)
            {
                db.Consignments.Add(consignment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // GET: Consignments1/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = await db.Consignments.FindAsync(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // POST: Consignments1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "consignmentId,stock,costPrice,salePrice,Size,Color,styleNumber,consignmentBrandId")] Consignment consignment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consignment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.consignmentId = new SelectList(db.Products, "productId", "productName", consignment.consignmentId);
            return View(consignment);
        }

        // GET: Consignments1/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consignment consignment = await db.Consignments.FindAsync(id);
            if (consignment == null)
            {
                return HttpNotFound();
            }
            return View(consignment);
        }

        // POST: Consignments1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Consignment consignment = await db.Consignments.FindAsync(id);
            db.Consignments.Remove(consignment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
