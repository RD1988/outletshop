﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Outletshop.Models.Interface;
using Outletshop.Models.Repository;
using Outletshop.Models.Database;
using System.Threading.Tasks;
using System.Net;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Threading;

namespace Outletshop.Controllers
{
    public class ProductsController : Controller
    {
        private Connection db = new Connection();

        IProductsRepository _repository;

        public ProductsController() : this(new ProductsRepository())
        {

        }

        public ProductsController(IProductsRepository repository)
        {
            _repository = repository;
        }

        // GET: Products
        public ActionResult Index()
        {
            ViewData["ControllerName"] = this.ToString();
            return View("Index", _repository.GetAllProducts());
        }

       



        public ActionResult View(int? id)
        {
           

            var query =  (from b in db.Products where b.productId == id select b).ToList();

            foreach (var item in query)
            {
                ViewBag.getStock = Convert.ToInt32(item.consignment.stock);

                // int getStock = Convert.ToInt32(item.consignment.stock);

                //ViewBag.getStock = getStock;

                if (ViewBag.getStock > 1)
                {
                    // ViewBag.Message = "På lager ";

                    var list = new List<int>();
                    for (int i = 1; i <= ViewBag.InStock; i++)
                    {

                        list.Add(i);

                    }

                    SelectList selectedList = new SelectList(list);
                    ViewBag.DdList = selectedList.ToString();



                }

                else if (ViewBag.getStock <= 0)
                {
                    ViewBag.Message = "Ikke på lager";
                }

                // foreach ends
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }



            Product pro =  _repository.GetProductById(id);



            if (pro == null)
            {
                return HttpNotFound();
            }

            return View("View", pro);
            
        }

        [HttpPost, ActionName("View")]
        public ActionResult View(int? id, string quantity)
        {
            quantity = Request.Form["quantity"];
            Session["Qty"] = quantity;

            ViewBag.Session = Session["Qty"];


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var StockToUpdate =  _repository.GetProductById(id);

            try
            {
                if (string.IsNullOrEmpty(Session["quantity"] as string))
                {
                    ViewBag.Message = "Kurvenen er tom!";
                }

                else
                {
                   

                    List<Item> Quantity = (List<Item>)Session["Qty"];

            


                }

   

            }
            catch (Exception)
            {


                throw;
            }

            return View(StockToUpdate);
        }
    }

 


    //    public ActionResult Create()
    //    {
    //        return View("Create");
    //    }

    //    [HttpPost]
    //    public ActionResult Create(ConsignmentProduct viewModel, string filename)
    //    {


    //        try
    //        {
    //            if (ModelState.IsValid)
    //            {
    //                _repository.CreateConsignment(viewModel, filename);
    //                return RedirectToAction("ProductList");
    //            }

    //        }
    //        catch (Exception ex)
    //        {

    //            ModelState.AddModelError("", ex);
    //            ViewData["CreateError"] = "Unable to create; view innerexception";
    //        }

    //        return View("Create");
    //    }
}



