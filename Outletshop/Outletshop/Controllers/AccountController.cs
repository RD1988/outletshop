﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Outletshop.Models.Database;
using Outletshop.Models.Interface;
using Outletshop.Models.Repository;
using System.IO;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace Outletshop.Controllers
{
    [Authorize(Roles = "1")]
    public class AccountController : Controller
    {

        IProductsRepository _repository;

        public AccountController() : this(new ProductsRepository())
        {

        }

        public AccountController(IProductsRepository repository)
        {
            _repository = repository;
        }

        // GET: Account
        public ActionResult Index()
        {


            return View();
        }


        // GET: Account
        public ActionResult Dashboard()
        {
            ViewBag.Username = User.Identity.Name;

            return View();
        }

        protected override void HandleUnknownAction(string actionName)
        {
            this.View(actionName).ExecuteResult(this.ControllerContext);
            //base.HandleUnknownAction(actionName);
        }



        [Authorize(Roles = "1")]
        public ActionResult ProductList()
        {
            ViewData["ControllerName"] = this.ToString();
            return View("ProductList",  _repository.GetAllProducts());
        }

    

        public ActionResult Create()
        {

            PopulateBrandsDropDownList();

            return View("Create");
        }


        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {

                        var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Products", Server.MapPath(@"\")));

                        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "imagepath");

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);

                        string filename = "test";

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }


     

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(ConsignmentProduct viewModel, string filename)
        {
            ViewBag.Username = User.Identity.Name;

            try
            {
                if (ModelState.IsValid)
                {
                    
                    _repository.CreateConsignment(viewModel, filename);
                    SaveUploadedFile();
                    PopulateBrandsDropDownList(viewModel.brandId);
                    return RedirectToAction("ProductList");
                }

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex);
                ViewData["CreateError"] = "Unable to create; view innerexception";
            }

            return View("Create");
        }


        public ActionResult Delete(int? id)
        {
        

            var delete = _repository.GetConsignmentById(id);
            return View();

        }

        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            try
            {
                _repository.DeleteConsignment(id);
                return RedirectToAction("ProductList");
            }
            catch (Exception)
            {
    
                return View();
            }
        }



        private void PopulateBrandsDropDownList(object selectedBrand = null)
        {
            Connection db = new Connection();

            var brandsQuery = from b in db.Brands
                                   orderby b.brandName
                                   select b;
            ViewBag.brandId = new SelectList(brandsQuery, "brandId", "brandName", selectedBrand);
        }

    }
}