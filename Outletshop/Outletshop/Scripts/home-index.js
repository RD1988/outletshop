﻿(function ($) {
    function HomeIndex() {
        var $this = this;

        function initialize()
        {
            $('#productDescription').summernote(
             {
                focus: true,
                height: 300,
                codemirror: {
                    theme: 'united'
                }


             });

            $(".summernote").summernote({
                styleWithSpan: false,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol']]
                ]
            });
     


        }

        $this.init = function () {
            initialize();
        }
    }
    $(function () {
        var self = new HomeIndex();
        self.init();
    })
}(jQuery))