﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using System.Web.Security;
using Outletshop.Models.Database;
using System.Linq;
using System.Text.RegularExpressions;

namespace Outletshop
{
    public class MvcApplication : System.Web.HttpApplication
    {

        private const String ReturnUrlRegexPattern = @"\?ReturnUrl=.*$";

        public MvcApplication()
        {
            PreSendRequestHeaders += MvcApplicationOnPreSendRequestHeaders;
        }

        private void MvcApplicationOnPreSendRequestHeaders(object sender, EventArgs e)
        {
            String redirectUrl = Response.RedirectLocation;

            if (String.IsNullOrEmpty(redirectUrl) || !Regex.IsMatch(redirectUrl, ReturnUrlRegexPattern))
            {

                return;

            }

            Response.RedirectLocation = Regex.Replace(redirectUrl, ReturnUrlRegexPattern, String.Empty);


        }


        //protected void Session_Start(object sender, EventArgs e)
        //{
        //    Session["SessionLock"] = new object();
        //}


        protected void Application_Start()
        {
            //Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<Outletshop.Models.Database.Connection>());

            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DbContext>());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {

            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string accountMail = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        using (Connection db = new Connection())
                        {
                            Account account = db.Account.SingleOrDefault(a => a.accountMail == accountMail);
                            roles = account.role.RoleId.ToString();
                        }

                         e.User = new System.Security.Principal.GenericPrincipal(
                         new System.Security.Principal.GenericIdentity(accountMail, "Forms"), roles.Split(';'));


                    }
                    catch (Exception)
                    {

                       // throw;
                    }
                }
            }

        }

    }
}
